### <img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="25px"> Hey 

♥️ Coucou izay mijery 😙😙

## Who am I?
 ```python
 class WhoAmI:
 	user = 'Toavina Mi'
	current_edu = "Any Antanimena any eh"
	hobbies = [
				'Sleeping',
				'Watching Anime',
				'Eating food'
				'Being up all Night chasing that ONE BUG...'
			]
	
	def getCity():
		return TNR_MDG()
	
	def Ambitions():
		BecomeRich() 💰
		Pilot() ✈️
		FindLove() 💑
	
 ```
 

## :computer: Technologies
* Machine Learning
* Mobile App Development
* Frontend and Backend Web Development


[![Toavina's GitHub stats](https://github-readme-stats.vercel.app/api?username=mitoavina&count_private=true&show_icons=true&theme=radical&hide_border)](https://github.com/mitoavina/github-readme-stats)


## Programming Languages
<img src = 'https://github.com/MarikIshtar007/MarikIshtar007/blob/master/images/c-original.svg' width='30'/> <img src = 'https://github.com/MarikIshtar007/MarikIshtar007/blob/master/images/cpp.svg' width='30'/> <img src = 'https://github.com/MarikIshtar007/MarikIshtar007/blob/master/images/pycharm.svg' width='30'/> <img src = 'https://github.com/MarikIshtar007/MarikIshtar007/blob/master/images/python2.png' height='30'/> <img src = 'https://github.com/MarikIshtar007/MarikIshtar007/blob/master/images/flutter-logo.svg' width='30'/> <img src = 'https://github.com/MarikIshtar007/MarikIshtar007/blob/master/images/html.svg' width='30'/> <img src = 'https://github.com/MarikIshtar007/MarikIshtar007/blob/master/images/css.svg' width='30'/> <img src = 'https://github.com/MarikIshtar007/MarikIshtar007/blob/master/images/js.svg' width='30'/> <img src = 'https://github.com/MarikIshtar007/MarikIshtar007/blob/master/images/bootstrap.svg' width='33'/> <img src = 'https://github.com/MarikIshtar007/MarikIshtar007/blob/master/images/django.svg' height='40'/> <img src = 'https://github.com/MarikIshtar007/MarikIshtar007/blob/master/images/flask.png' width='30'/> <img src = 'https://github.com/MarikIshtar007/MarikIshtar007/blob/master/images/php.svg' width='40'/>
 <img src = 'https://github.com/MarikIshtar007/MarikIshtar007/blob/master/images/sql.svg' width='30'/> <img src = 'https://github.com/MarikIshtar007/MarikIshtar007/blob/master/images/git.svg' width='30'/>
 
 [![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=mitoavina&layout=compact)](https://github.com/mitoavina/github-readme-stats)


Last Edited on: 08/09/2021

[![Gmail Badge](https://img.shields.io/badge/-toavinakwely.tsymandeha@gmail.com-c14438?style=flat-square&logo=Gmail&logoColor=white&link=mailto:toavina@gmail.com)](mailto:toavina@gmail.com)
